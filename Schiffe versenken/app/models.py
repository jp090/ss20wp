from django.db import models
from . import views

# Create your models here.

class Player(models.Model):
    txt = models.TextField(blank=False, null=False, max_length=23)
    
    def __str__(self):
        if self.txt:
            txt_str = 'X'
        return "[{}] {} ({})".format(txt_str, self.txt)
        
        
class Sound(models.Model):
    onoff = models.CharField(max_length=23)
    on = models.BooleanField()

